<?php

/* Do not remove this line. */
require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');


/*
 * adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="theme-color" content="#ffe01b">

  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/GT-America-Expanded-Medium.OTF" as="font" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/GT-America-Medium.OTF" as="font" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/GT-America-Regular.OTF" as="font" crossorigin="anonymous">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

<?php }

add_action('wp_head', 'scratch_meta');

/* Theme CSS */

function theme_styles() {

  wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css', false, null );

  wp_enqueue_style( 'plyr', 'https://cdn.plyr.io/3.5.6/plyr.css', false, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  if (!is_admin()) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', array(), null, true );
  }

  wp_enqueue_script( 'barba', 'https://cdnjs.cloudflare.com/ajax/libs/barba.js/1.0.0/barba.min.js', array(), null, true );

  wp_enqueue_script( 'gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js', array(), null, true );

  wp_enqueue_script( 'plyr', 'https://cdnjs.cloudflare.com/ajax/libs/plyr/3.5.6/plyr.min.js', array(), null, true );

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true ); 

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/build/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/build/main.min.js'), true );


  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));
}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

// Image sizes
add_image_size('selected-cases', 550, 656, true);
add_image_size('blog', 550, 656, true);
add_image_size('cases', 880, 549, true);

// Add created images sizes to dropdown in WP control panel
add_filter( 'image_size_names_choose', 'custom_image_sizes' );

function custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array( 
    
  ) );
} 

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'scratch-main-nav' => __( 'Main Nav', 'scratch' ),  // main nav in header
    'scratch-contact-nav' => __( 'Contact Nav', 'scratch' )
  )
);

function scratch_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */

function scratch_contact_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Contact Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-contact-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */


// Register Custom Post Type Case
function create_case_cpt() {

  $labels = array(
    'name' => _x( 'Cases', 'Post Type General Name', 'lionlab' ),
    'singular_name' => _x( 'Case', 'Post Type Singular Name', 'lionlab' ),
    'menu_name' => _x( 'Cases', 'Admin Menu text', 'lionlab' ),
    'name_admin_bar' => _x( 'Case', 'Add New on Toolbar', 'lionlab' ),
    'archives' => __( 'Case Archives', 'lionlab' ),
    'attributes' => __( 'Case Attributes', 'lionlab' ),
    'parent_item_colon' => __( 'Parent Case:', 'lionlab' ),
    'all_items' => __( 'Alle Cases', 'lionlab' ),
    'add_new_item' => __( 'Tilføj ny Case', 'lionlab' ),
    'add_new' => __( 'Tilføj ny', 'lionlab' ),
    'new_item' => __( 'Ny Case', 'lionlab' ),
    'edit_item' => __( 'Rediger Case', 'lionlab' ),
    'update_item' => __( 'Update Case', 'lionlab' ),
    'view_item' => __( 'Se Case', 'lionlab' ),
    'view_items' => __( 'Se Cases', 'lionlab' ),
    'search_items' => __( 'Søg Case', 'lionlab' ),
    'not_found' => __( 'Not found', 'lionlab' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
    'featured_image' => __( 'Featured Image', 'lionlab' ),
    'set_featured_image' => __( 'Set featured image', 'lionlab' ),
    'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
    'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
    'insert_into_item' => __( 'Insert into Case', 'lionlab' ),
    'uploaded_to_this_item' => __( 'Uploaded to this Case', 'lionlab' ),
    'items_list' => __( 'Cases list', 'lionlab' ),
    'items_list_navigation' => __( 'Cases list navigation', 'lionlab' ),
    'filter_items_list' => __( 'Filter Cases list', 'lionlab' ),
  );
  $rewrite = array(
    'slug' => 'referencer',
    'with_front' => true,
    'pages' => true,
    'feeds' => true,
  );
  $args = array(
    'label' => __( 'Case', 'lionlab' ),
    'description' => __( '', 'lionlab' ),
    'labels' => $labels,
    'menu_icon' => 'dashicons-archive',
    'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'author', 'page-attributes', 'post-formats'),
    'taxonomies' => array('category', 'cases'),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'can_export' => true,
    'has_archive' => true,
    'hierarchical' => false,
    'exclude_from_search' => false,
    'show_in_rest' => true,
    'publicly_queryable' => true,
    'capability_type' => 'post',
    'rewrite' => $rewrite,
  );
  register_post_type( 'case', $args );

}
add_action( 'init', 'create_case_cpt', 0 );


/* Place custom functions below here. */

/* Don't delete this closing tag. */
?>
