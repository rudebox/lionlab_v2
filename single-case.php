<?php 
  get_template_part('parts/header'); the_post(); 

  //section settings
  $margin = get_field('margin');

  //intro
  $intro_text = get_field('cases_intro');
  $title = get_field('header');
  $meta_title = get_field('meta_header');
?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="single-case padding--<?php echo esc_attr($margin); ?>">
    <div class="wrap hpad">
      <div class="bg--gray-dark single-case__intro">
        
          <h5 class="single-case__meta-title meta-title"><?php echo esc_html($meta_title); ?></h5>
          <h2 class="single-case__header title"><?php echo $title; ?></h2>
          
          <div class="row">
            <div class="col-sm-6">

              <?php 
                //cases services repeater field group layout
                if (have_rows('cases_services') ) :
              ?>

              <div class="row">

                <?php 
                  while (have_rows('cases_services') ) : the_row(); 
                    $service_name = get_sub_field('cases_services_name');
                ?>

                <div class="col-sm-4 single-case__service-column">
                  <h6 class="single-case__service-title title"><?php echo esc_html($service_name); ?></h6>
                  
                  <ul>
                    <?php 
                      //cases services repeater field group layout
                      if (have_rows('cases_services_subname') ) : while (have_rows('cases_services_subname') ) : the_row(); 

                        $name = get_sub_field('name');
                    ?>

                      <li class="gray-medium"><?php echo esc_html($name); ?></li>

                    <?php endwhile; endif; //cases_service_subname end loops ?>
                  </ul>
                </div>

                <?php endwhile; //cases services repeater end while loop ?>

              </div>

            <?php endif; //cases services repeater end if loop ?>
          </div>

          <div class="col-sm-5 col-sm-offset-1">
              <?php echo $intro_text; ?>
          </div>

        </div>
      </div>
      </div>
    </section>

    <?php 
      //cases showacse images repeater field group layout
      if (have_rows('cases_img') ) :

      //counter
      $i=0;
    ?>

    <section class="single-case is-animated is-animated--fadeUp padding--both">

      <div class="wrap hpad">
        <div class="row">
          
          <?php 
            while (have_rows('cases_img') ) : the_row(); 
              $img = get_sub_field('img');

            $i++;

            if ($i === 2) :
              $class = 'col-sm-offset-2 single-case__showcase-img--offset'; 
            else:
              $class = '';
            endif;
          ?>

          <div class="col-sm-5 <?php echo esc_attr($class); ?>">
            <img src="<?php echo $img['sizes']['selected-cases'] ?>" alt="<?php echo $img['alt']; ?>">
          </div>

          <?php endwhile; //cases showacse images end while loop ?>

        </div>
      </div>
    </section>

    <?php endif; //cases showacse images end if loop ?>

    <?php 
        //cases results repeater field group
        if (have_rows('cases_results') ) : 
          $title = get_field('cases_results_header');
    ?>
    <section class="single-case is-animated is-animated--fadeUp padding--both">
      <div class="wrap hpad">
        <div class="row">

          <div class="col-sm-6">
            <h2 class="title"><?php echo esc_html($title); ?></h2>
            <a class="btn btn--yellow" href="/kontakt">Kontakt</a>
          </div>
          
          <div class="col-sm-6">
            <?php 
              while (have_rows('cases_results') ) : the_row(); 
                $result = get_sub_field('result');
                $result_text = get_sub_field('result_text');
            ?>

              <div class="col-sm-4 single-case__results">
                <strong class="h2"><?php echo esc_html($result); ?>%</strong>
                <p class="gray-medium"><?php echo esc_html($result_text); ?></p>
              </div>

            <?php endwhile; //cases results end while loop ?>
          </div>
        </div>
      </div>
    </section>
    <?php endif; //cases results end if loop ?>
    
    <?php 
      $type = get_field('cases_media_type');
      $large_img = get_field('cases_large_img');
      $video_teaser = get_field('cases_video_teaser');
      $video_mp4 = get_field('cases_video_mp4');
      $video_ogv = get_field('cases_video_ogv');
      $video_webm = get_field('cases_video_webm');
     ?>
    
    <section class="single-case padding--both">
      <div class="wrap hpad">
        <div class="row">

          <?php if ($type === 'img') : ?>
          <div class="col-sm-12">
            <img src="<?php echo esc_url($large_img['url']); ?>" alt="<?php echo $large_img['alt']; ?>">
          </div>
          <?php endif; ?>
          
          <?php if ($type === 'video') : ?>
            <div class="col-sm-12">
              <video class="video__video video__video--preview" preload="auto" autoplay loop muted="muted" volume="0">
                <source src="<?php echo esc_url($video_teaser); ?>" type="video/mp4" codecs="avc1, mp4a">
                Your browser doesn't support HTML5 video. upgrade your browser to improve your experience.
              </video>

              <video id="player" class="video__video video__video--full-length" preload="auto" loop muted="muted" volume="0">
                <source src="<?php echo esc_url($video_mp4); ?>" type="video/mp4" codecs="avc1, mp4a">
                <source src="<?php echo esc_url($video_ogv); ?>" type="video/ogg" codecs="theora, vorbis">
                <source src="<?php echo esc_url($video_webm); ?>" type="video/webm" codecs="vp8, vorbis">
                Your browser doesn't support HTML5 video. upgrade your browser to improve your experience.
              </video>
            </div>
           <?php endif; ?> 

        </div>
      </div>
    </section>

    <section class="padding--both">
    <?php get_template_part('parts/contact'); ?>
    </section>

</main>

<?php get_template_part('parts/footer'); ?>