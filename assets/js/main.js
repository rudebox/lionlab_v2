$(document).ready(function() {

  function plyr() {
    
      var player = new Plyr('.video__video--full-length', {muted: true, volume: 0});

      //hide video thumb on 
      $('.plyr__control').on('click', function() {
        $('.video__video--preview ').addClass('is-hidden');
        $('.video__video--preview ').css({
          'visibility': 'hidden',
           'opacity': 0
        })
      });
  }

  plyr();

  //toggle contact forms
  function showForm() {
    $('#js-btn-2').on('click', function() {
      $('#js-form-2').toggleClass('is-active');
      $('#js-form-3').removeClass('is-active');
      $('#js-form-4').removeClass('is-active');
      $('.contact__area').toggle();
    });

    $('#js-btn-3').on('click', function() {
      $('#js-form-2').removeClass('is-active');
      $('#js-form-3').toggleClass('is-active');
      $('#js-form-4').removeClass('is-active');
      $('.contact__area').toggle();
    });

    $('#js-btn-4').on('click', function() {
      $('#js-form-2').removeClass('is-active');
      $('#js-form-4').toggleClass('is-active');
      $('#js-form-3').removeClass('is-active');
      $('.contact__area').toggle();
    });

    $('.contact__reset').on('click', function() {
      $('.contact__form').removeClass('is-active');
      $('.contact__area').toggle();
    });
  }

  showForm();


  //inertia scrollTope
  function inertiaScroll() {
    
  jQuery(function($) {

      "use strict";

      var win = $(window)
          , target = $('body')
          , wrapper = target.find('.js-scrollable')
          , easing = 'cubic-bezier(0.19, 1, 0.22, 1)' //css easing
          , duration = '1.2s' //duration ms(millisecond) or s(second)
          , top = 0
          , kineticScroll = {
              _init: function() {
                  if( wrapper.length == 1 ) {
                      target.height(wrapper.height());
                      wrapper.css({
                          transition: 'transform ' + duration + ' ' + easing,
                          position: 'fixed',
                          top: '0',
                          left: '0',
                          width: '100%'
                      });

                      win.on({
                          scroll: function () {
                              kineticScroll._scroll();
                          }
                          , resize: function() {
                              target.height(wrapper.outerHeight());
                          }
                      });

                      kineticScroll._scroll();
                  }
              },
              _scroll: function() {
                  top = win.scrollTop();
                  wrapper.css('transform', 'translateY(-' + top + 'px) translateZ(0)');
                  target.height(wrapper.outerHeight());
              }
          };

      if (typeof window.ontouchstart == 'undefined') {
          kineticScroll._init();
      }
      });
  
  }

  inertiaScroll();


  //in viewport check
  function viewportCheck() {
    var $animation_elements = $('.is-animated');
    var $window = $(window);

    function check_if_in_view() {
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
     
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
     
        //check to see if this current container is within viewport
        if ((element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');
  }

  viewportCheck();


  //contact toggle
  function contactToggle() {
    $('.contact__toggle').click(function(e) {
      e.preventDefault();

      $('.contact__title').toggleClass('is-active');      
      $('.contact__areas').toggleClass('is-active');

       $('.contact__area').each(function(index) {        
          var delayNumber = index * 100;
          
          $(this).delay(delayNumber).queue(function(next) {
            $(this).toggleClass('is-active');
            next();
          });  

      })
    });
  }

  contactToggle(); 


  //toggle bullshit
  function toggleBullshit() {
    $('.bullshit__toggle input').click(function() {

      //modal      
      $('#modal, #overlay').addClass('is-active');

      setTimeout(function() {
        $('#modal, #overlay').removeClass('is-active');
      },3000);

      //terms to be highlighted
      var terms = ['prisvindende', 'awards'];

      function highlight(that) {
          that.find('.title, .meta-title').each(function(){
              var full_text = $(this).html(),
                  element = $(this);
              $.each(terms, function(i){
                  full_text = full_text.replace(terms[i], "<s class='bullshit__terms'>"+terms[i]+"</s>");
              });
              element.html(full_text);
          });
      }

      highlight($('body'));

    });
  }

  toggleBullshit(); 


  //btn wrap
  function btnWrap() {
    $('.btn--gray, .btn--yellow').wrapInner('<span></span>');
  }

  btnWrap();

  //outside of transition wrapper
  function btnHeaderWrap() {
    $('.nav__item--btn a').wrapInner('<span></span>');
  }

  btnHeaderWrap();

  //filter animation
  function filter() {
    $('.blog__filter--dropdown').on('click', function() {
      // $('.blog__filter').toggleClass('is-active');
      $(this).toggleClass('is-active'); 
      $('.blog__filter').each(function(index) {        
          var delayNumber = index * 100;
          
          $(this).delay(delayNumber).queue(function(next) {
            $(this).toggleClass('is-active');
            next();
          });  

      })
    }); 
  }

  filter();


  function placeholderFocus() {
    $('.newsletter__form-input').focus(function() {
      $(this).attr('placeholder', 'Indtast din E-mail adresse');
    }).blur(function() {
      $(this).attr('placeholder', 'Tilmeld dig vores nyhedsbrev');
    });
  }

  placeholderFocus(); 


  //menu toggle
  function menuToggle() {
    $('.nav-toggle').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
    });
  }

  menuToggle();


  //mixit up
  function mixit ()  {
  if($('body').is('.blog')) {

      // Instantiate and configure the mixer
      var mixer = mixitup('.mixit', {
        
      });

    }
  }

  mixit();
  


  //
  // Barba.js
  //
  Barba.Pjax.Dom.wrapperId = 'route-transition';
  Barba.Pjax.Dom.containerClass = 'route-transition__container';
  Barba.Pjax.ignoreClassLink = 'no-ajax';

  var general = Barba.BaseView.extend({
    namespace: 'common',
    onEnter: function() {
      // The new Container is ready and attached to the DOM.
    },
    onEnterCompleted: function() {
      // The Transition has just finished.
      $('.nav__link').removeClass('is-disable');
      $('.header__logo').removeClass('is-active');
      $('.js-scrollable').removeClass('is-loading');
      $('.route-transition__wrap, .route-transition__text').removeClass('is-active');
      $('.page__title').addClass('in-view');  
    },
    onLeave: function() {
      // A new Transition toward a new page has just started.
      $('.nav__link').addClass('is-disable');
      $('.header__logo').addClass('is-active');
      $('.js-scrollable').addClass('is-loading'); 
      $('.route-transition__wrap, .route-transition__text').addClass('is-active');
      $(window).scrollTop(0); // scroll to top here 
    },
    onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.

    }
  });


  general.init();

  Barba.Pjax.start();

  Barba.Prefetch.init(); 

  Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

  Barba.Pjax.preventCheck = function(evt, element) {
    if (!Barba.Pjax.originalPreventCheck(evt, element)) {
      return false;
    }

    //outcomment if you want to prevent barba when logged in
    // if ($('body').hasClass('logged-in')) { Barba.Pjax.preventCheck = function() { 
    //   return false;  
    // }; } 

    // No need to check for element.href -
    // originalPreventCheck does this for us! (and more!)
    if (/.pdf/.test(element.href.toLowerCase())) {
      return false;
    } else if (/edit/.test(element.href.toLowerCase())) {
      return false;
    } else if (/wp-admin/.test(element.href.toLowerCase())) {
      return false;
    }

    return true;
  };

  var CommonTransition = Barba.BaseTransition.extend({
  start: function() {
  /**
  * This function is automatically called as soon the Transition starts
  * this.newContainerLoading is a Promise for the loading of the new container
  * (Barba.js also comes with an handy Promise polyfill!)
  */
  // As soon the loading is finished and the old page is faded out, let's fade the new page
  Promise
  .all([this.newContainerLoading, this.fadeOut()])
  .then(this.fadeIn.bind(this));
  },
  fadeOut: function() { 

  /**
  * this.oldContainer is the HTMLElement of the old Container
  */
  //return $(this.oldContainer).animate({ opacity: 0 }).promise();
  return $(this.oldContainer).animate({ opacity: 0 }).promise();
  },
  fadeIn: function() {
    /**
    * this.newContainer is the HTMLElement of the new Container
    * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
    * Please note, newContainer is available just after newContainerLoading is resolved!
    */
    var _this = this;
    var $newContainer = $(this.newContainer);

    $(this.oldContainer).hide();

    $newContainer.css({
      visibility: 'visible',
      opacity: 0
    });


    setTimeout(function () {    
      
      $newContainer.animate({
        opacity: 1,
      }, 1000, function() {

        _this.done();

      });

    }, 1000);
    }

  });
  /**
  * Next step, you have to tell Barba to use the new Transition
  */
  Barba.Pjax.getTransition = function() {
  /**
  * Here you can use your own logic!
  * For example you can use different Transition based on the current page or link...
  */
  return CommonTransition;

  };

  Barba.Dispatcher.on('initStateChange', function() {

    menuToggle();
    $('body').removeClass('is-fixed');
    $('.nav--mobile').removeClass('is-open');
    $('.nav__dropdown').removeClass('is-visible');
  });

  /**
  * GET WP body classes
  */
  Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {

    // html head parser borrowed from jquery pjax
    var $newPageHead = $( '<head />' ).html(
        $.parseHTML(
            newPageRawHTML.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]
          , document
          , true
        )
    );

    var headTags = [
        "meta[name='keywords']"
      , "meta[name='description']"
      , "meta[property^='og']"
      , "meta[name^='twitter']"
      , "meta[itemprop]"
      , "link[itemprop]"
      , "link[rel='prev']"
      , "link[rel='next']"
      , "link[rel='canonical']"
    ].join(',');
    $( 'head' ).find( headTags ).remove(); // Remove current head tags
    $newPageHead.find( headTags ).appendTo( 'head' ); // Append new tags to the head 


    //update body classes
    var response = newPageRawHTML.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', newPageRawHTML)
    var bodyClasses = $(response).filter('notbody').attr('class')
    $('body').attr('class', bodyClasses);
  
    menuToggle();
    contactToggle();
    viewportCheck();
    placeholderFocus();
    mixit();
    plyr();
    var player = new Plyr('.video__video--full-length', {muted: true, volume: 0});
    btnWrap();
    inertiaScroll();
    showForm();

    if ($('body').is('.blog')) {
      filter();
    }


    //get current url and path
    var $path = currentStatus.url.split(window.location.origin)[1].substring(1); 
    var $url = currentStatus.url;

    //add active class based upon URL status
    $('.nav__item a').each(function() {  
      if ($url === this.href) {
        $(this).addClass('is-active');
      }

      else {
        $(this).removeClass('is-active');
        $('.nav__item').removeClass('is-active');
      }
    });


  }); 

  Barba.Dispatcher.on('transitionCompleted', function (currentStatus, oldStatus, container) {
    
  });

});