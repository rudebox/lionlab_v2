</div>
</div>


<?php 
	//contact informations
	$phone 	   = get_field('phone', 'options');
	$mail  	   = get_field('mail', 'options');
	$cvr   	   = get_field('cvr', 'options');
	$address   = get_field('address', 'options');

	//certs
	$title = get_field('cert_title', 'options');
	$text = get_field('cert_text', 'options');
	$number = get_field('cert_number', 'options');
 ?>

<footer class="footer bg--gray-dark" id="footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="wrap hpad">

		<div class="footer__wrap flex flex--justify flex--wrap">
			<div class="footer__wrap--inner">
				<h5 class="meta-title">Kontakt</h5>
				<a class="h4" href="mailto:<?php echo esc_attr($mail); ?>"><?php echo esc_html($mail); ?></a>
				<br><a class="h4" href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a>
			</div>

			<div class="footer__certs footer__wrap--inner gray-medium col-sm-7 flex flex--column flex--justify">
				<h5 class="meta-title"><?php echo esc_html($title); ?> (<span class="yellow"><?php echo esc_html($number); ?></span>)</h5>
					<?php echo $text; ?>
			</div>
		</div>

		<div class="row flex flex--wrap">

			<div class="col-sm-5 footer__contact-info gray-medium">
				<p>CVR: <?php echo esc_html($cvr); ?></p>
				<?php echo $address; ?>
			</div>

			<div class="col-sm-7 footer__menu flex flex--wrap">	
				<div class="row footer__row flex flex--wrap">
					<?php 
						if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
							the_row();
						$title = get_sub_field('column_title');
						$text = get_sub_field('column_text');
					 ?>

					 <div class="col-sm-4 footer__column gray-medium">
					 	<?php if ($title) : ?>
					 	<h5 class="footer__title meta-title"><?php echo esc_html($title); ?></h4>
					 	<?php endif; ?>
						<?php echo $text; ?>
					 </div>

					<?php endwhile; endif; ?>
				 </div>			
			</div>

		</div>
	</div>
</footer>
</div>

<div id="overlay" aria-hidden="true"></div>
<div id="modal" aria-hidden="true">
	<h4>Bullshit filter aktiveret</h4>
</div>

<?php wp_footer(); ?>

</body>
</html>
