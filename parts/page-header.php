<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//pageheader variables
	$add_img = get_field('add_img');
	$show_icon = get_field('show_icon');
	$page_img = get_field('page_img');

	//post bg
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
?>

<section class="page__hero">
	<div class="wrap hpad page__container">
		<div class="row">

			<div class="col-sm-11 col-sm-offset-1">
				<div class="page__title-wrap">
					<h1 class="page__title is-animated is-animated--reveal"><?php echo $title; ?></h1>
				</div>
				<?php echo file_get_contents('wp-content/themes/lionlab_v2/assets/img/lion.svg'); ?>
			</div>

		</div>
		<?php if (!is_post_type_archive() ) : ?>
		<?php if ($add_img === true || $show_icon === true || is_single() ) : ?>
			<!-- <?php echo file_get_contents('wp-content/themes/lionlab_v2/assets/img/scroll.svg'); ?> --> 
		<?php endif; ?>
		<?php endif; ?>
	</div>
</section>

<?php if ($add_img === true && !is_post_type_archive() ) : ?>
	<div class="page__img-container wrap hpad">
		<div class="page__img--wrapper">
			<div class="page__img is-animated is-animated--zoomIn" style="background-image: url(<?php echo esc_url($page_img['url']); ?>);">
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if (is_singular('post') ) : ?>
	<div class="page__img-container wrap hpad">
		<div class="page__img--wrapper">
			<div class="page__img is-animated is-animated--zoomIn" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></div>
		</div>
	</div>
<?php endif; ?>