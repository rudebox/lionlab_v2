<?php 
/**
* Description: Lionlab large-text field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');
$meta_title = get_sub_field('header_meta'); 
$title = get_sub_field('header');
$text = get_sub_field('large_text'); 
$medium_text = get_sub_field('medium_text'); 

if ($medium_text === true ) {
  $medium_text = 'large-text__col--medium';
}
?>

<section class="large-text padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
  <div class="wrap hpad">
    <?php if ($meta_title) : ?>
      <h5 class="large-text__meta-title meta-title"><?php echo esc_html($meta_title); ?></h5>
    <?php endif; ?>
    <?php if ($title) : ?>
      <h2 class="large-text__title"><?php echo esc_html($title); ?></h2>
    <?php endif; ?>
    <div class="row">

      <div class="col-sm-7 large-text__col <?php echo esc_attr($medium_text); ?>">
        <?php echo $text; ?>
      </div>

    </div>
  </div>
</section>
