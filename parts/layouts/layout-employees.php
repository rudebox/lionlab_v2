<?php 
/**
* Description: Lionlab employees repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$meta_title = get_sub_field('header_meta');

if (have_rows('employee') ) :
?>

<section class="employee padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($meta_title) : ?>
		<h5 class="employee__meta-title meta-title"><?php echo esc_html($meta_title); ?></h5>
		<?php endif; ?>
		
		<?php if ($title) : ?>
		<h2 class="employee__header title"><?php echo $title; ?></h2>
		<?php endif; ?>

		<div class="row flex flex--wrap">
			<?php while (have_rows('employee') ) : the_row(); 
				$name = get_sub_field('employee_name');
				$img = get_sub_field('employee_img');
				$mail = get_sub_field('employee_mail');
				$phone = get_sub_field('employee_phone');
				$position = get_sub_field('employee_position');
			?>

			<div class="col-sm-3 employee__item is-animated is-animated--fadeUp">
				<picture>
					<img src="<?php echo esc_url($img['sizes']['blog']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				</picture>
				<span class="employee__wrap">
					<h4 class="employee__name is-animated"><?php echo esc_html($name); ?></h4>
				</span>
				<span class="employee__wrap">
					<p class="employee__position label is-animated"><?php echo $position; ?></p>
				</span>
				<br>
				<span class="employee__wrap">
					<a class="btn--td is-animated" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
					<a class="btn--td is-animated" href="tel:<?php echo esc_html(get_formatted_phone($phone)); ?>"><?php echo esc_html($phone); ?></a>
				</span>
			</div>
			<?php endwhile; ?>
		</div>

	</div>
</section>
<?php endif; ?>