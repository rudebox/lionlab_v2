<?php 
/**
* Description: Lionlab footer pagination field group
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//layout and fallback from options page
$pagination_link = get_sub_field('pagination_link') ? : get_field('pagination_link', 'options');
$pagination_title = get_sub_field('pagination_title') ? : get_field('pagination_title', 'options');
$pagination_bg = get_sub_field('pagination_bg') ? : get_field('pagination_bg', 'options');
?>

<a href="<?php echo esc_url($pagination_link); ?>" class="footer-pagination is-animated">
	<div class="footer-pagination__bg" style="background-image: url(<?php echo esc_url($pagination_bg['url']); ?>);"></div>


	<div class="wrap hpad footer-pagination__container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 pagination__col center">
				<h5 class="footer-pagination__meta-title meta-title center">Næste side</h5>
				<h3 class="footer-pagination__title"><?php echo esc_html($pagination_title); ?></h3>
			</div>
		</div>
	</div>
</a>