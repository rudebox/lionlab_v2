<?php 
/**
* Description: Lionlab WYSIWYGS repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings
$margin = get_sub_field('margin');
$meta_title = get_sub_field('header_meta');

global $layout_count; ?>
<section id="wysiwygs-<?php echo esc_attr($layout_count); ?>" class="wysiwygs padding--<?php echo esc_attr($margin); ?>">
  <div class="wrap hpad">
    <?php if ($meta_title) : ?>
      <h5 class="wysiwygs__meta-title meta-title"><?php echo esc_html($meta_title); ?></h5>
    <?php endif; ?>
    <?php if(get_sub_field('header')): ?>
      <h2 class="wysiwygs__title"><?php echo esc_html(the_sub_field('header')); ?></h2>
    <?php endif; ?>
    <?php
    if(get_sub_field('offset') !== 'Flexible') {
        $flex = false;
        if(get_sub_field('offset') === '2 to 1') {
          $offset = '2:1';
        } else {
          $offset = '1:2';
        }
      } else {
        $flex = true;
        $offset = null;
      }
      scratch_layout_declare(get_sub_field('wysiwygs'), 2, $flex, $offset);
      while(has_sub_field('wysiwygs')) {
        scratch_layout_start();
          the_sub_field('wysiwyg');
        scratch_layout_end();
      }
    ?>
  </div>
</section>
