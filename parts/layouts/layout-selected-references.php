<?php 
/**
* Description: Lionlab selected references repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$meta_title = get_sub_field('header_meta');
$link = get_sub_field('link');
$link_text = get_sub_field('link_text');

if (have_rows('references') ) :

?>

<section class="references <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<h5 class="references__meta-title meta-title"><?php echo esc_html($meta_title); ?></h5>
		<h2 class="references__header title"><?php echo $title; ?></h2>
		<div class="row flex flex--wrap">

			<div class="col-sm-6">
				<?php if ($link) : ?>
				<a class="btn btn--yellow" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
				<?php endif; ?>
			</div>

			<div class="col-sm-6 references__list">
				<?php while (have_rows('references') ) : the_row(); 
					$title = get_sub_field('references_name');
					$link_type = get_sub_field('references_link_type');
					$internally_link = get_sub_field('references_internally_link');
					$extern_link = get_sub_field('references_extern_link');
				?>
				
				<?php if ($link_type === 'internally') : ?>
				<a href="<?php echo esc_url($internally_link); ?>" class="references__item flex flex--justify is-animated is-animated--fadeUp">
				<?php endif; ?>
				<?php if ($link_type === 'extern') : ?>
				<a target="_blank" href="<?php echo esc_url($extern_link); ?>" class="references__item flex flex--justify is-animated is-animated--fadeUp">
				<?php endif; ?>
					<h6 class="references__title title"><?php echo esc_html($title); ?></h6>
					<svg class="references__icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M295.6 163.7c-5.1 5-5.1 13.3-.1 18.4l60.8 60.9H124.9c-7.1 0-12.9 5.8-12.9 13s5.8 13 12.9 13h231.3l-60.8 60.9c-5 5.1-4.9 13.3.1 18.4 5.1 5 13.2 5 18.3-.1l82.4-83c1.1-1.2 2-2.5 2.7-4.1.7-1.6 1-3.3 1-5 0-3.4-1.3-6.6-3.7-9.1l-82.4-83c-4.9-5.2-13.1-5.3-18.2-.3z"/></svg>	
				</a>
				<?php endwhile; ?>

			</div>
		</div>
	</div>
</section>
<?php endif; ?>