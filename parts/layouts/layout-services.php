<?php 
/**
* Description: Lionlab services repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$meta_title = get_sub_field('header_meta');
$text = get_sub_field('header_text');

if (have_rows('service') ) :
?>

<section class="services padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		
		<?php if ($meta_title) : ?>
		<h5 class="services__meta-title meta-title center"><?php echo esc_html($meta_title); ?></h5>
		<?php endif; ?>
		
		<div class="row services__row">
			<div class="col-sm-6">
				<?php if ($title) : ?>
				<h2 class="services__header title"><?php echo $title; ?></h2>
				<?php endif; ?>
			</div>

			<div class="col-sm-6">
				<?php echo $text; ?>
			</div>
		</div>

		<div class="row flex flex--wrap">
			<?php while (have_rows('service') ) : the_row(); 
				$name = get_sub_field('service_name');
				$img = get_sub_field('services_img');
				$link = get_sub_field('service_link');
				$text = get_sub_field('service_text');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-3 services__item">
				<?php if ($img) : ?>
				<span class="services__img-wrap">
				</span>
				<div class="services__img" style="background-image: url(<?php echo esc_url($img['sizes']['selected-cases']); ?>">
					
				</div>
				<?php endif; ?>
				<div class="services__wrap">
					<h4 class="services__title"><?php echo esc_html($name); ?></h4>
					<span class="services__link label">Udforsk</span>
				</div>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>