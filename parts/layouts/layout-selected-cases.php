<?php 
/**
* Description: Lionlab selected cases repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('cases_box') ) :
?>

<section class="selected-cases padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h5 class="selected-cases__meta-title meta-title center"><?php echo esc_html($title); ?></h5>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('cases_box') ) : the_row(); 
				$title = get_sub_field('cases_title');
				$img = get_sub_field('cases_img');
				$link = get_sub_field('cases_link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-6 selected-cases__item is-animated">
				<?php if ($img) : ?>
				<span class="selected-cases__img-wrap">
					<img class="selected-cases__img" src="<?php echo esc_url($img['sizes']['cases']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				</span>
				<?php endif; ?>
				<div class="selected-cases__wrap">
					<h3 class="selected-cases__title is-animated"><?php echo esc_html($title); ?></h3>
					<span class="selected-cases__link label is-animated">Udforsk</span>
				</div>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>