<?php  
/**
* Description: Lionlab contact field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

?>

<section class="contact">
	<?php get_template_part('parts/contact'); ?>
</section>