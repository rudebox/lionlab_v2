<?php 
/**
* Description: Lionlab service overview repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$meta_title = get_sub_field('header_meta');

if (have_rows('service_overview') ) :
?>

<section class="services-overview padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		
		<?php if ($meta_title) : ?>
		<h5 class="services-overview__meta-title meta-title"><?php echo esc_html($meta_title); ?></h5>
		<?php endif; ?>
		

		<div class="row flex flex--wrap">
			<?php while (have_rows('service_overview') ) : the_row(); 
				$name = get_sub_field('service_overview_name');
				$img = get_sub_field('service_overview_img');
				$link = get_sub_field('service_overview_link');
				$text = get_sub_field('service_overview_text');
				$position = get_sub_field('service_overview_img_position');
			?>
			
			<div class="services-overview__item">

				<div class="col-sm-12 services-overview__img">
					<?php if ($img) : ?>
					<span class="services-overview__img-wrap">
					</span>
					<div class="services-overview__bg" style="background-image: url(<?php echo esc_url($img['url']); ?>">
						
					</div>
					<?php endif; ?>
				</div>
				
				<div class="col-sm-12 services-overview__text flex flex--wrap bg--gray-dark">
					<div class="col-sm-6">
						<h4 class="services-overview__title title"><?php echo esc_html($name); ?></h4> 
						<a class="btn btn--yellow services-overview__btn" href="<?php echo esc_url($link); ?>">Udforsk</a>
					</div>

					<div class="col-sm-6 services-overview__excerpt">
						<?php echo $text; ?>
					</div>
				</div>
			</div>

			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>