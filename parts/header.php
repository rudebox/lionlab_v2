<!doctype html>

<html <?php language_attributes(); ?> class="has-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<div class="js-scrollable">

<header class="header" id="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="wrap hpad flex flex--center flex--justify">

    <a class="header__logo" aria-label="logo" href="<?php bloginfo('url'); ?>">
      <?php echo file_get_contents('wp-content/themes/lionlab_v2/assets/img/logo.svg'); ?> 
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
        <div class="bullshit__toggle">
          <input type="checkbox" id="toggle-bullshit" /><label for="toggle-bullshit">Toggle</label>
        </div>
        <?php scratch_contact_nav(); ?> 
      </div>
    </nav>

  </div>
</header>



<div id="route-transition" class="transition">

<div class="route-transition__container" data-namespace="common"> 
