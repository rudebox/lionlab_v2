<?php 
 /**
* Description: Lionlab post navigation
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

 $next_post = get_next_post();
 $previous_post = get_previous_post();

 //trim post title to excerpt
 $excerpt = wp_trim_words($next_post->post_title, 6, '...');

 //next post bg
 $next_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($next_post->ID), 'url' );

 //previous post bg
 $previous_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($previous_post->ID), 'url' );

 if (!empty($next_post) ):
?>
<a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="footer-pagination is-animated">
<div class="footer-pagination__bg" style="background-image: url(<?php echo esc_url($next_thumb[0]); ?>);"></div>
	<div class="wrap hpad footer-pagination__container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 pagination__col center">
				<h5 class="footer-pagination__meta-title meta-title center">Næste indlæg</h5>
				<h3 class="footer-pagination__title"><?php echo $excerpt; ?></h3>
			</div>
		</div>
	</div>
</a>
<?php endif; ?>

<?php if (empty($next_post) ): ?>
<a href="<?php echo esc_url(get_permalink($previous_post->ID) ); ?>" class="footer-pagination is-animated">
<div class="footer-pagination__bg" style="background-image: url(<?php echo esc_url($previous_thumb[0]); ?>);"></div>
	<div class="wrap hpad footer-pagination__container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 pagination__col center">
				<h5 class="footer-pagination__meta-title meta-title center">Næste indlæg</h5>
				<h3 class="footer-pagination__title"><?php echo esc_attr( $next_post->post_title ); ?></h3>
			</div>
		</div>
	</div>
</a>
<?php endif; ?>