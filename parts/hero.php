<?php 
	$title = get_field('hero_title'); 
	$add_hero = get_field('add_hero');
	$hero_img = get_field('hero_img');
?>

<section class="page__hero flex">
	<div class="page__container wrap hpad flex">
		<div class="row page__row flex flex--center">

			<div class="col-sm-11 col-sm-offset-1">
				<div class="page__title-wrap">
					<h1 class="page__title is-animated is-animated--reveal"><?php echo $title; ?></h1>
				</div>
				<?php echo file_get_contents('wp-content/themes/lionlab_v2/assets/img/lion.svg'); ?>
			</div>

		</div>
		<!-- <?php echo file_get_contents('wp-content/themes/lionlab_v2/assets/img/scroll.svg'); ?> -->
	</div>
</section>
<?php if ($add_hero === true) : ?>
	<div class="page__img--wrapper is-animated is-animated--fadeUp">
		<div class="page__img page__img--hero is-animated is-animated--zoomIn" style="background-image: url(<?php echo esc_url($hero_img['url']); ?>);"></div>
	</div>
<?php endif; ?>