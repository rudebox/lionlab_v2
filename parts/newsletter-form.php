<div class="wrap hpad padding--both">
	<form id="js-newsletter-form" action="https://lionlab.us18.list-manage.com/subscribe/post?u=0ed04b9902d368430d5882ce3&id=a04b25c056" method="POST">
        
        <div class="newsletter__form-field">
			 <div class="flex flex--wrap newsletter__form-wrap is-animated">
		     	<input class="newsletter__form-input" type="email" placeholder="Tilmeld dig vores nyhedsbrev" autocapitalize="off" autocomplete="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" value="">
		     	<label>
			     	<input type="submit" class="newsletter__form-submit" name="submit" onfocus="this.placeholder = 'Skriv din e-mail'" onblur="this.placeholder = 'enter your text'">
			     	<svg class="newsletter__form-btn" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M295.6 163.7c-5.1 5-5.1 13.3-.1 18.4l60.8 60.9H124.9c-7.1 0-12.9 5.8-12.9 13s5.8 13 12.9 13h231.3l-60.8 60.9c-5 5.1-4.9 13.3.1 18.4 5.1 5 13.2 5 18.3-.1l82.4-83c1.1-1.2 2-2.5 2.7-4.1.7-1.6 1-3.3 1-5 0-3.4-1.3-6.6-3.7-9.1l-82.4-83c-4.9-5.2-13.1-5.3-18.2-.3z"/></svg>
		     	</label>
		     </div>            
        </div>
		
		<input type="hidden" name="mc_signupsource" value="hosted">
	</form>
</div>