<?php 
/**
* Description: Lionlab contact field group options page
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


$title = get_field('contact_form_title', 'options');
?>

<div class="wrap hpad">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 contact__options">
			<button class="contact__toggle"><h2 class="contact__title is-animated"><span><?php echo esc_html($title); ?></span> <span></span></h2></button>
			<ul class="contact__areas">
				<?php if (have_rows('contact_areas', 'options') ) : 
					while (have_rows('contact_areas', 'options') ) : the_row(); 

					$area = get_sub_field('contact_area');
					$form_id = get_sub_field('contact_form_id');
				?>

				<li class="contact__area"><button class="contact__btn" id="js-btn-<?php echo esc_attr($form_id); ?>"><?php echo esc_html($area); ?></button>
				</li>

				<div class="contact__form" id="js-form-<?php echo esc_attr($form_id); ?>">
					<svg class="contact__reset" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M295.6 163.7c-5.1 5-5.1 13.3-.1 18.4l60.8 60.9H124.9c-7.1 0-12.9 5.8-12.9 13s5.8 13 12.9 13h231.3l-60.8 60.9c-5 5.1-4.9 13.3.1 18.4 5.1 5 13.2 5 18.3-.1l82.4-83c1.1-1.2 2-2.5 2.7-4.1.7-1.6 1-3.3 1-5 0-3.4-1.3-6.6-3.7-9.1l-82.4-83c-4.9-5.2-13.1-5.3-18.2-.3z"/></svg>
					<?php gravity_form( $form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>

				<?php endwhile; endif; ?>

			</ul>
		</div>
	</div>
</div>