<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'video' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'video' ); ?>

    <?php
    } elseif( get_row_layout() === 'selected-cases' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'selected-cases' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    } elseif( get_row_layout() === 'selected-references' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'selected-references' ); ?>

    <?php
    } elseif( get_row_layout() === 'services' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'services' ); ?>

    <?php
    } elseif( get_row_layout() === 'footer-pagination' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'footer-pagination' ); ?>

    <?php
    } elseif( get_row_layout() === 'large-text' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'large-text' ); ?>

    <?php
    } elseif( get_row_layout() === 'employees' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'employees' ); ?>

    <?php
    } elseif( get_row_layout() === 'contact' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'contact' ); ?>

    <?php
    } elseif( get_row_layout() === 'service-overview' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'service-overview' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
