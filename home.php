<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="blog padding--bottom">
    <div class="wrap hpad padding--bottom">
      <?php get_template_part('parts/blog', 'filter'); ?>
      <div class="row flex flex--wrap">

          <div class="mixit blog__row flex flex--wrap">

            <?php if (have_posts()): ?>
              <?php while (have_posts()): the_post(); ?>

                 <?php 
                $cats = get_the_category();
                $cat_string = "";

                foreach ($cats as $cat) {
                  $cat_string .= " cat" . $cat->term_id ."";
                }
            ?>

            <?php 
                //post img
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'blog' );
                //post img alt tag
                $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);   
             ?>

            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="blog__post col-sm-3 is-animated is-animated--fadeUp mix <?php echo $cat_string; ?>" itemscope itemtype="http://schema.org/BlogPosting">
              <picture>
                <img itemprop="thumbnail" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">
              </picture>

              <header>
                <span class="blog__cat label"><?php echo esc_html($cat->name); ?></span>
                <h2 class="blog__title h5" itemprop="headline">                                
                    <?php the_title(); ?>
                </h2>
              </header>

              <div class="blog__excerpt gray-medium" itemprop="articleBody">
                <?php the_excerpt(); ?>
              </div>

              <span class="btn--td blog__btn">Læs mere</span>

            </a>

            <?php endwhile; else: ?>

              <p>No posts here.</p>

          <?php endif; ?>
        </div>

      </div>
    </div>

    <?php get_template_part('parts/newsletter', 'form'); ?>

  </section>

  <?php get_template_part('parts/layouts/layout', 'footer-pagination'); ?>

</main>

<?php get_template_part('parts/footer'); ?>