<?php
 /**
   * Description: Lionlab helpers
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	//remove countrycode from phone
	function get_formatted_phone($str) {
	  
	  // Remove +45
	  $str = str_replace('+', '00', $str);

	  // Only allow integers
	  $str = preg_replace('/[^0-9]/s', '', $str);

	  return $str;
	}


	//allow svg uploads
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');

	//allow webp uploads
	function webp_upload_mimes( $existing_mimes ) {
	// add webp to the list of mime types
	$existing_mimes['webp'] = 'image/webp';

	// return the array back to the function with our added mime type
	return $existing_mimes;
	}

	add_filter( 'mime_types', 'webp_upload_mimes' );



	// Custom excerpt length
	function custom_excerpt_length( $length ) {
	  return 20;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// Custom excerpt text
	function custom_excerpt_more( $more ) {
	  return '&hellip;';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');


	// Move Yoast to bottom
	function yoasttobottom() {
	  return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


	//get proper title
	function get_proper_title( $id = null  ) {
	  
	  if ( is_null($id) ) {
	    global $post;
	    $id = $post->ID;
	  }

	  $acf_title = get_field('page_title', $id);

	  return ($acf_title) ? $acf_title : get_the_title( $id );
	}

	//get author gravatar
	function my_gravatar_url() { 

		$user_email = get_the_author_meta( 'user_email' );
		// Convert email into md5 hash and set image size to 80 px
		$user_gravatar_url = 'http://www.gravatar.com/avatar/' . md5($user_email) . '?s=80';
		echo $user_gravatar_url; 
	}

	//make gf submit input to button
	function gf_make_submit_input_into_a_button_element($button_input, $form) {

	  //save attribute string to $button_match[1]
	  preg_match("/<input([^\/>]*)(\s\/)*>/", $button_input, $button_match);

	  //remove value attribute
	  $button_atts = str_replace("value='".$form['button']['text']."' ", "", $button_match[1]);

	  return '<button '.$button_atts.'><span>'.$form['button']['text'].'</span></button>';
	}

	add_filter('gform_submit_button', 'gf_make_submit_input_into_a_button_element', 10, 2);
 
?>