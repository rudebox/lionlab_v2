<?php

/*
 * Template Name: Ydelser
 */

get_template_part('parts/header'); the_post();

//section settings
$margin = get_field('margin');

//intro
$intro_text = get_field('service_intro');
$title = get_field('header');
$meta_title = get_field('meta_header');
?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="services padding--<?php echo esc_attr($margin); ?>">
    <div class="wrap hpad padding--bottom is-animated is-animated--fadeUp">
      <h5 class="single-case__meta-title meta-title"><?php echo esc_html($meta_title); ?></h5>
      <h2 class="single-case__header title"><?php echo $title; ?></h2>

        <div class="row">
          <div class="col-sm-6">

            <?php 
              //services repeater field group layout
              if (have_rows('service_services') ) :
            ?>

            <div class="row">

              <?php 
                while (have_rows('service_services') ) : the_row(); 
                  $service_name = get_sub_field('service_services_name');
              ?>

              <div class="col-sm-4 services__service-column">
                <h6 class="services__service-title title"><?php echo esc_html($service_name); ?></h6>
                
                <ul>
                  <?php 
                    //services repeater field group layout
                    if (have_rows('service_services_subname') ) : while (have_rows('service_services_subname') ) : the_row(); 

                      $name = get_sub_field('name');
                  ?>

                    <li class="gray-medium"><?php echo esc_html($name); ?></li>

                  <?php endwhile; endif; //service_subname end loops ?>
                </ul>
              </div>

              <?php endwhile; //services repeater end while loop ?>

            </div>

          <?php endif; // services repeater end if loop ?>
        </div>

        <div class="col-sm-5 col-sm-offset-1">
            <?php echo $intro_text; ?>
        </div>

      </div>

    </div>

    <?php 
      //service partners repeater field group layout
      if (have_rows('service_partners') ) :
      	$header = get_field('service_partners_header');
    ?>

	    <div class="wrap hpad padding--top">
	    	<h2 class="title"><?php echo esc_html($header); ?></h2>
		    <div class="row flex flex--wrap">

		      <?php 
		        while (have_rows('service_partners') ) : the_row(); 
		          $service_logo = get_sub_field('service_logo');
		      ?>

		      <div class="col-sm-3 services__partners flex flex--hvalign is-animated is-animated--fadeUp">
		         <img src="<?php echo esc_url($service_logo['url']) ?>" alt="<?php echo esc_attr($service_logo['alt']); ?>">
		      </div>

		      <?php endwhile; //service partners repeater end while loop ?>

		    </div>
	    </div>
    <?php endif; // service partners repeater end if loop ?>

  </section>

  <?php get_template_part('parts/content', 'layouts'); ?>

</main>

<?php get_template_part('parts/footer'); ?>