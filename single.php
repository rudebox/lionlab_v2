<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="blog-post padding--both">
    <div class="wrap hpad">
      <div class="row">

        <article class="col-sm-8 col-sm-offset-2" itemscope itemtype="http://schema.org/BlogPosting">
          
          <?php $author = get_the_author(); ?>
          <?php $facebook = get_the_author_meta( 'facebook', $post->post_author ); ?>
          
          <div class="blog-post__author flex flex--justify flex--wrap">

            <div class="blog-post__author-info">
              <picture class="blog-post__author-img">
                <source srcset="<?php echo my_gravatar_url(); ?>">
                <img src="<?php echo my_gravatar_url(); ?>" alt="<?php echo esc_html($author); ?>">
              </picture>
              <div class="blog-post__author-wrap">
                <div class="blog-post__author-name"><?php echo esc_html($author); ?></div>
                <p class="blog-post__author-description"><?php echo (get_the_author_meta('description') ); ?></p>
              </div>

            </div>
          
            <div class="blog-post__author-social">
              <a target="_blank" class="btn--td" href="<?php echo esc_url(get_the_author_meta( 'facebook', $post->post_author ) ); ?>">Facebook</a>

              <a target="_blank" class="btn--td" href="<?php echo esc_url(get_the_author_meta( 'instagram', $post->post_author ) ); ?>">Instagram</a>
              
              <a target="_blank" class="btn--td" href="<?php echo esc_url(get_the_author_meta( 'linkedin', $post->post_author ) ); ?>">Instagram</a>
            </div>

          </div>

          <div class="blog-post__content" itemprop="articleBody">
            <?php the_content(); ?>
          </div>

        </article>

      </div>
    </div>

    <?php get_template_part('parts/newsletter', 'form'); ?>
  </section>

  <?php get_template_part('parts/post', 'navigation'); ?>

</main>

<?php get_template_part('parts/footer'); ?>