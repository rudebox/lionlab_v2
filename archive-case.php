<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="case">
    <div class="wrap hpad">
      <div class="row">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

            <?php 
                //post img
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'cases' );
                //post img alt tag
                $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);  
            ?>

            <a href="<?php the_permalink(); ?>" class="col-sm-6 cases__item is-animated is-animated--fadeUp">

              <?php if ($thumb) : ?>
                <img src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">
              <?php endif; ?>
              <div class="cases__wrap">
                <h3 class="cases__title"><?php the_title(); ?></h3>
                <span class="cases__link label">Udforsk</span>
              </div>

            </a>

          <?php endwhile; else: ?>

            <p>No posts here.</p>

        <?php endif; ?>

      </div>      
    </div>

  </section>

  <section class="contact">
    <?php get_template_part('parts/contact'); ?>
  </section>

  <?php $pagination_bg = get_sub_field('pagination_bg') ? : get_field('pagination_bg', 'options'); ?>

  <a href="/losninger" class="footer-pagination is-animated">
    <div class="footer-pagination__bg" style="background-image: url(<?php echo esc_url($pagination_bg['url']); ?>);"></div>


    <div class="wrap hpad footer-pagination__container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 pagination__col center">
          <h5 class="footer-pagination__meta-title meta-title center">Næste side</h5>
          <h3 class="footer-pagination__title">Løsninger</h3>
        </div>
      </div>
    </div>
  </a>

</main>

<?php get_template_part('parts/footer'); ?>